import React from 'react';

class Popular extends React.Component {
  render() {
    return(
      <div className="popular panel">
        <h1>Most Viewed</h1>
        <div className="video-grid">
          {
            this.props.popular.map( video =>
              <div className="video" key={video.vid}>
                <div className="thumbnail"><img src={video.image_url} alt={video.title} /></div>
                <div className="title">{video.title}</div>
                <div className="date">{video.date}</div>
              </div>
            )
          }
        </div>
      </div>
    )
  }
}

export default Popular;
