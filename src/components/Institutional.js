import React from 'react';

class Institutional extends React.Component {
  render() {
    return(
      <div className="institutional panel">
        <h1>Institutional</h1>
        <div className="video-grid">
          {
            this.props.institutional.map( video =>
              <div className="video" key={video.vid}>
                <div className="thumbnail"><img src={video.image_url} alt={video.title} /></div>
                <div className="title">{video.title}</div>
                <div className="date">{video.date}</div>
              </div>
            )
          }
        </div>
      </div>
    )
  }
}

export default Institutional;
