import React, { Component } from 'react';
import axios from 'axios';
import Banner from './Banner';
import Latest from './Latest';
import Popular from './Popular';
import Institutional from './Institutional';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      latest: [],
      popular: [],
      institutional: [],
      banner: []
    };

  }

  componentDidMount() {
    const id = "2240";
    const url = `https://titan.asset.tv/api/channel-view-json/${id}`;

    axios.get(url)
      .then(resp => {
        this.setState({
          latest: resp.data.content['713'],
          popular: resp.data.content['952'],
          institutional: resp.data.content['6101'],
          banner: resp.data.mcd
        } );
      });
  };

  render() {
    return (
      <div>
        <div className="banner">
          <Banner banner={this.state.banner} />
        </div>
        <div className="video-grid">
          <Latest latest={this.state.latest} />
          <Popular popular={this.state.popular} />
          <Institutional institutional={this.state.institutional} />
        </div>
    </div>
    );
  }
}

export default App;
