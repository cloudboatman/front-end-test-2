import React from 'react';

class Latest extends React.Component {
  render() {
    return(
      <div className="latest panel active-panel">
        <h1>Latest</h1>
        <div className="video-grid">
          {
            this.props.latest.map( video =>
              <div className="video" key={video.vid}>
                <div className="thumbnail"><img src={video.image_url} alt={video.title} /></div>
                <div className="title">{video.title}</div>
                <div className="date">{video.date}</div>
              </div>
            )
          }
        </div>
      </div>
    )
  }
}

export default Latest;
