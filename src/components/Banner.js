import React from 'react';

class Banner extends React.Component {

  showLatest(e) {
    const tabs = document.querySelectorAll('.tab');
    const panels = document.querySelectorAll('.panel')
    tabs.forEach(tab => {
      tab.classList.remove('active-tab');
    })
    panels.forEach(panel => {
      panel.classList.remove('active-panel');
    })
    this.latest.classList.add('active-tab');
    document.querySelector('.latest').classList.add('active-panel');
  }

  showPopular(e) {
    const tabs = document.querySelectorAll('.tab');
    const panels = document.querySelectorAll('.panel')
    tabs.forEach(tab => {
      tab.classList.remove('active-tab');
    })
    panels.forEach(panel => {
      panel.classList.remove('active-panel');
    })
    this.popular.classList.add("active-tab");
    document.querySelector('.popular').classList.add('active-panel');
  }

  showInstitutional(e) {
    const tabs = document.querySelectorAll('.tab');
    const panels = document.querySelectorAll('.panel')
    tabs.forEach(tab => {
      tab.classList.remove('active-tab');
    })
    panels.forEach(panel => {
      panel.classList.remove('active-panel');
    })
    this.institutional.classList.add("active-tab");
    document.querySelector('.institutional').classList.add('active-panel');
  }

  render() {
    const banner = this.props.banner.hdr_bg_1;
    const logo = this.props.banner.channel_header_logo;
    return (
      <div>
      <img className="banner" src={banner} />
      <img className="logo" src={logo} />
      <div className="tabs">
        <div className="tab latest-tab active-tab" onClick={(e) => this.showLatest(e)} ref={ el => this.latest = el }>Latest</div>
        <div className="tab popular-tab" onClick={(e) => this.showPopular(e)} ref={ el => this.popular = el }>Popular</div>
        <div className="tab institutional-tab" onClick={(e) => this.showInstitutional(e)} ref={ el => this.institutional = el }>Institutional</div>
      </div>
      </div>
    )
  }
}

export default Banner;
